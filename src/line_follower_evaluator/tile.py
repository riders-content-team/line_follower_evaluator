from __future__ import division

from line_follower_evaluator.evaluatortype import EvaluatorType
from line_follower_evaluator.leftevaluator import LeftEvaluator
from line_follower_evaluator.rightevaluator import RightEvaluator
from line_follower_evaluator.sharpleftevaluator import SharpLeftEvaluator
from line_follower_evaluator.sharprightevaluator import SharpRightEvaluator
from line_follower_evaluator.lineevaluator import LineEvaluator


class Tile:

    def __init__(self, evaluatorType, trackName, order, x, y, theta):

        self.trackName = trackName
        self.order = order
        self.locked = False

        if evaluatorType == 0:
            self.evaluator = LeftEvaluator(x, y, theta)
        if evaluatorType == 1:
            self.evaluator = RightEvaluator(x, y, theta)
        elif evaluatorType == 2:
            self.evaluator = LineEvaluator(x, y, theta)
        elif evaluatorType == 3:
            self.evaluator = SharpLeftEvaluator(x, y, theta)
        elif evaluatorType == 4:
            self.evaluator = SharpRightEvaluator(x, y, theta)

    def update(self, position, prev_position):
        if not self.locked:
            self.evaluator.update(position, prev_position)

    def get_sum_area(self):
        return self.evaluator.get_sum_area()

    def get_character(self):
        return self.evaluator.get_character()

    def lock_tile(self):
        self.locked = True